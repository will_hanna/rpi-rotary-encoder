int i2c_initialize(int bus_num);
int i2c_address_select (int addr);
int i2c_data_send (char* buffer, int length );
int i2c_data_recv (int length, char* buffer );