##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=RPi-zero-programming3
ConfigurationName      :=Debug
WorkspacePath          :=/home/william/Desktop/DSC/RPi-zero-programming3/RPi-zero-programming3
ProjectPath            :=/home/william/Desktop/DSC/RPi-zero-programming3/RPi-zero-programming3/RPi-zero-programming3
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=William
Date                   :=04/01/21
CodeLitePath           :=/home/william/.codelite
LinkerName             :=/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++
SharedObjectLinkerName :=/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="RPi-zero-programming3.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/usr/lib $(IncludeSwitch)/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/lib/arm-linux-gnueabihf/ $(IncludeSwitch)/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/usr/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)wiringPi 
ArLibs                 :=  "wiringPi" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch). $(LibraryPathSwitch)/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/usr/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /opt/cross-pi-gcc/bin/arm-linux-gnueabihf-ar rcu
CXX      := /opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++
CC       := /opt/cross-pi-gcc/bin/arm-linux-gnueabihf-gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /opt/cross-pi-gcc/bin/arm-linux-gnueabihf-as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IntermediateDirectory)/rpi_i2c.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)


$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.c$(ObjectSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main.c$(DependSuffix) -MM main.c
	$(CC) $(SourceSwitch) "/home/william/Desktop/DSC/RPi-zero-programming3/RPi-zero-programming3/RPi-zero-programming3/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.c$(PreprocessSuffix) main.c

$(IntermediateDirectory)/rpi_i2c.c$(ObjectSuffix): rpi_i2c.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/rpi_i2c.c$(ObjectSuffix) -MF$(IntermediateDirectory)/rpi_i2c.c$(DependSuffix) -MM rpi_i2c.c
	$(CC) $(SourceSwitch) "/home/william/Desktop/DSC/RPi-zero-programming3/RPi-zero-programming3/RPi-zero-programming3/rpi_i2c.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/rpi_i2c.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/rpi_i2c.c$(PreprocessSuffix): rpi_i2c.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/rpi_i2c.c$(PreprocessSuffix) rpi_i2c.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


