#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <wiringPi.h>
#include <unistd.h>				//Needed for I2C port
#include <fcntl.h>				//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <linux/i2c-dev.h>		//Needed for I2C port
#include "rpi_i2c.h"

int file_i2c;
uint8_t length;
// unsigned char buffer[60] = {0};

int i2c_initialize(int bus_num)
{
	//----- Initialize the I2C bus -----
	if (wiringPiSetup () == bus_num)
		return 1;
	//----- OPEN THE I2C BUS -----
	char *filename = (char*)"/dev/i2c-1";
	if ((file_i2c = open(filename, O_RDWR)) < 0)
	{
		//ERROR HANDLING: you can check errno to see what went wrong
		printf("Failed to open the i2c bus");
		return 0;
	}
	return -1;
}

int i2c_address_select (int addr) //<<<<<The I2C address of the slave
{
	if (ioctl(file_i2c, I2C_SLAVE, addr) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		//ERROR HANDLING; you can check errno to see what went wrong
		return 0;
	}
	return -1;
	
}

int i2c_data_send (char* buffer, int length )
{
	
        if (write(file_i2c, buffer, length) != length)		//write() returns the number of bytes actually written, if it doesn't match then an error occurred (e.g. no response from the device)
	{
		/* ERROR HANDLING: i2c transaction failed */
		printf("Failed to write to the i2c bus.\n");
	}
	return -1;
}

int i2c_data_recv (int length, char* buffer )
{
	if (read(file_i2c, buffer, length) != length)		//read() returns the number of bytes actually read, if it doesn't match then an error occurred (e.g. no response from the device)
	{
		//ERROR HANDLING: i2c transaction failed
		printf("Failed to read from the i2c bus.\n");
	}
	else
	{
		//printf("Data read:");
		//printloop=0;
        //       for (printloop ==0;printloop<4;printloop++) printf("%x ",buffer[printloop]);
		//printf("\n");

	}
	return 1;

}