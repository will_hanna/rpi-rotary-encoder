#include <stdio.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>				//Needed for I2C port
#include <fcntl.h>				//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <linux/i2c-dev.h>		//Needed for I2C port
//#include "rpi_i2c.h"
#define ADC_CONFIG_BYTE 0x61
#define ADC_SLAVE_ADDR 0x3C // OLED display


// the event counter 
volatile int eventCounter = 0;

// -------------------------------------------------------------------------
// myInterrupt:  called every time an event occurs
void myInterrupt(void) {
   eventCounter++;
}

int main(int argc, char **argv)
{
	printf("hello world\n");
	
    
    i2c_initialize(01);
    //char data_send[60] = {0};
	//	// unsigned char data_recv[60] = {0};
	//	int data_length=0;
	//	i2c_address_select (ADC_SLAVE_ADDR);  // ADC slave address
	//	data_send[0] = ADC_SETUP_BYTE;		// ADC read channel 5 (VBATs)
	//	data_send[1] = (ADC_CONFIG_BYTE | (channel <<1)); // leftshirt channel to align with CS bits
	//	data_length = 2;			//<<< Number of bytes to write
	//	i2c_data_send (data_send,data_length );
	//	i2c_data_recv (data_length, data_recv);
    i2c_address_select (ADC_SLAVE_ADDR);
    uint8_t screen_buffer[128]={0};
    uint8_t command_buffer[16]={0x00,0};
    uint8_t clear_packet[2]={0x40,0};
    uint8_t screen[128][8] ={{0,0}};						// holds the screen bytes as pages uint8_t page=0;
    uint8_t pageflag=0;										// pageflag - bit for page set to '1' if changed data, '0' means no need to update.(if update called with pageflag=0 update all as a precaution
    uint8_t page=0;
    int8_t tik=0;
    
    #define DATA_LENGTH_OLED_SETUP 27

	static uint8_t OLED_setup[DATA_LENGTH_OLED_SETUP] = {
	0x00,0xAE,0x02,0x10,0x40,0xB0,0x81,0x80,0xA1,0xA6,0xA8,0x3F,0xAD,0x8B,0x30,0xC8,0xD3,0x00,0xD5,0x80,0xD9,0x1F,0xDA,0x12,0xDB,0x40,0xAF,};

	i2c_data_send(OLED_setup,DATA_LENGTH_OLED_SETUP);
    
    
	uint8_t cnt_loc=0;
	uint8_t x_offset=1;
	if (pageflag==0)
	{
		for (page=0;page<8;page++)
		{
			command_buffer[0]=0x00;
			command_buffer[1]=0x02; //column lower bit
			command_buffer[2]=0x10; //column upper bit
			command_buffer[3]=0xB0+page;
			i2c_data_send(command_buffer,4);
			for (cnt_loc=0;cnt_loc<128;cnt_loc++)
			screen_buffer[cnt_loc+x_offset]= 0x50;//0x55 for lines //screen[cnt_loc][page];
			screen_buffer[0]=0x40;
			i2c_data_send(screen_buffer,129);
		}
	}
// #######################################
// ##
// ## Rotary encoder setup 
// ##
// #######################################
// #
// GPIO switch 0
// GPIO OUTA 2
// GPIO OUTB 3
int pin1 = 0;
int clkState = 0;
int oldClkState =0 ;
int dtState = 0;
int counter = 0;

pinMode(0, INPUT);
pullUpDnControl(0, PUD_OFF);
pinMode(2, INPUT);
pullUpDnControl(2, PUD_OFF);
pinMode(3, INPUT);
pullUpDnControl(3, PUD_OFF);

if ( wiringPiISR (0, INT_EDGE_FALLING, &myInterrupt) < 0 ) {
      fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno));
      return 1;
  }

if ( wiringPiISR (2, INT_EDGE_FALLING, &myInterrupt) < 0 ) {
      fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno));
      return 1;
  }  
  
if ( wiringPiISR (3, INT_EDGE_FALLING, &myInterrupt) < 0 ) {
      fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno));
      return 1;
  }  

while (1)
   {
    pin1 = digitalRead(0);
    clkState = digitalRead(2);
    dtState = digitalRead(3);
    while (eventCounter)
    {
        pin1 = digitalRead(0);
        clkState = digitalRead(2);
        dtState = digitalRead(3);
        if (clkState != oldClkState)
        {
            if (dtState != clkState)
            {
                counter++;
            }
            else
            {
                counter--;
            }
        }
        eventCounter = 0;
    oldClkState=clkState;
   printf("%d   %d   %d   %d\n", pin1 , clkState , dtState, counter);
   }
   }
}
