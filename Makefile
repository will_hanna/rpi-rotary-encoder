.PHONY: clean All

All:
	@echo "----------Building project:[ RPi-zero-programming3 - Debug ]----------"
	@cd "RPi-zero-programming3" && "$(MAKE)" -f  "RPi-zero-programming3.mk"
clean:
	@echo "----------Cleaning project:[ RPi-zero-programming3 - Debug ]----------"
	@cd "RPi-zero-programming3" && "$(MAKE)" -f  "RPi-zero-programming3.mk" clean
